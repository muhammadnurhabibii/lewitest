/*
SQLyog Trial v13.1.8 (64 bit)
MySQL - 10.4.22-MariaDB : Database - nama
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`nama` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `nama`;

/*Table structure for table `artis` */

DROP TABLE IF EXISTS `artis`;

CREATE TABLE `artis` (
  `artis` char(100) DEFAULT NULL,
  `nama_artis` char(100) DEFAULT NULL,
  `jk` char(100) DEFAULT NULL,
  `bayaran` char(100) DEFAULT NULL,
  `award` char(100) DEFAULT NULL,
  `Negara` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `artis` */

insert  into `artis`(`artis`,`nama_artis`,`jk`,`bayaran`,`award`,`Negara`) values 
('A001','Robert Downey Jr','Pria','0000000','2','AS'),
('A002','Angelina Jolie','Wanita','7000000','1','AS'),
('A003','JACKIE CHAN','PRIA','20000000','7','HK'),
('A004','JOE TASLIM','PRIA','35000000','1','ID');

/*Table structure for table `film` */

DROP TABLE IF EXISTS `film`;

CREATE TABLE `film` (
  `KD_FILM` char(100) DEFAULT NULL,
  `NM_FILM` char(100) DEFAULT NULL,
  `GENRE` char(100) DEFAULT NULL,
  `ARTIS` char(100) DEFAULT NULL,
  `PRODUSER` char(100) DEFAULT NULL,
  `PENDAPATAN` char(100) DEFAULT NULL,
  `NOMINASI` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `film` */

insert  into `film`(`KD_FILM`,`NM_FILM`,`GENRE`,`ARTIS`,`PRODUSER`,`PENDAPATAN`,`NOMINASI`) values 
('F001','IROM MAN2','0001','A001','PD01','200000000','2'),
('F004','SPIDERMAN HOME COMING','0001','A001','PD01','200000000','0'),
('F003','AVANGER CIVIL WAR','0001','A001','PD01','200000000','1'),
('F004','SPIDERMAN HOME COMING','0001','A001','PD01','200000000','0'),
('F004','SPIDERMAN HOME COMING','0001','A001','PD01','200000000','0'),
('F004','SPIDERMAN HOME COMING','0001','A001','PD01','200000000','0'),
('F004','SPIDERMAN HOME COMING','0001','A001','PD01','200000000','0'),
('F004','SPIDERMAN HOME COMING','0001','A001','PD01','200000000','0'),
('F004','SPIDERMAN HOME COMING','0001','A001','PD01','200000000','0'),
('F004','SPIDERMAN HOME COMING','0001','A001','PD01','200000000','0'),
('F004','SPIDERMAN HOME COMING','0001','A001','PD01','200000000','0'),
('F011','RUSH HOUR','G003','A003','PD05','69500000','5'),
('F012','KUNGFU PANDA','G003','A003','PD05','92300000','2'),
('F013','IRON MAN','G001','A001','PDO1','200000000','1');

/*Table structure for table `film_benar` */

DROP TABLE IF EXISTS `film_benar`;

CREATE TABLE `film_benar` (
  `kd_film` char(100) DEFAULT NULL,
  `nm_film` char(100) DEFAULT NULL,
  `genre` char(100) DEFAULT NULL,
  `artis` char(100) DEFAULT NULL,
  `produser` char(55) DEFAULT NULL,
  `pendapatan` char(55) DEFAULT NULL,
  `nominasi` int(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `film_benar` */

insert  into `film_benar`(`kd_film`,`nm_film`,`genre`,`artis`,`produser`,`pendapatan`,`nominasi`) values 
('F001','iron man','G001','A001','PD01','200000000',3),
('F002','IRON MAN 2','G001','A001','PD01','180000000',2),
('F003','IRON MAN 3','G001','A001','PD01','120000000',0),
('F004','AVENGER CIVIL WAR','G001','A001','PD01','200000000',1),
('F005','SOIDERMAN HOMECOMING','G001','A001','PD01','130000000',0),
('F006','THE RAID','G003','A004','PD03','80000000',5),
('F007','FAST AND FORIOUS','G004','A004','PD04','83000000',2),
('F008','HABIBIE DAN AINUN','G001','A005','PD03','67000000',4),
('F009','POLICE STORY','G001','A003','PD02','70000000',3),
('F010','POLICE STORY','G001','A003','PD02','71000000',1),
('F011','POLICE STORY 3','G001','A003','PD02','61500000',0),
('F012','RUSH HOUR','G003','AD03','PD05','69500000',2),
('F013','KUNGFU PANDA','G003','AD03','PD05','92300000',5);

/*Table structure for table `genre` */

DROP TABLE IF EXISTS `genre`;

CREATE TABLE `genre` (
  `kd_genre` char(100) DEFAULT NULL,
  `nm_genre` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `genre` */

insert  into `genre`(`kd_genre`,`nm_genre`) values 
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRAILER'),
('G006','FICTION');

/*Table structure for table `kota` */

DROP TABLE IF EXISTS `kota`;

CREATE TABLE `kota` (
  `id` int(11) DEFAULT NULL,
  `nama` char(100) DEFAULT NULL,
  `id_propinsi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `kota` */

insert  into `kota`(`id`,`nama`,`id_propinsi`) values 
(1,'jakarta',1),
(2,'bandung',2),
(3,'sumedang',3),
(4,'makasar',4),
(5,'surabaya',5),
(6,'medan',6);

/*Table structure for table `negara` */

DROP TABLE IF EXISTS `negara`;

CREATE TABLE `negara` (
  `kd_negara` char(100) DEFAULT NULL,
  `nm_negara` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `negara` */

insert  into `negara`(`kd_negara`,`nm_negara`) values 
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA');

/*Table structure for table `pegawai` */

DROP TABLE IF EXISTS `pegawai`;

CREATE TABLE `pegawai` (
  `id_pegawai` int(100) DEFAULT NULL,
  `umur` char(100) DEFAULT NULL,
  `nama` char(100) DEFAULT NULL,
  `status` char(100) DEFAULT NULL,
  `gapok` char(100) DEFAULT NULL,
  `tunjangan` char(100) DEFAULT NULL,
  `gaji_terima` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pegawai` */

insert  into `pegawai`(`id_pegawai`,`umur`,`nama`,`status`,`gapok`,`tunjangan`,`gaji_terima`) values 
(3,'30','nugroho','T','3000','2000','7000'),
(1,'22','bagus','M','3000','0','5000'),
(2,'21','budi','M','3000','0','5000'),
(5,'12','dino','M','3000','0','5000'),
(0,'0','dono','M','3000','0','5000');

/*Table structure for table `produser` */

DROP TABLE IF EXISTS `produser`;

CREATE TABLE `produser` (
  `kd_produser` char(50) DEFAULT NULL,
  `nm_produser` char(50) DEFAULT NULL,
  `internasional` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `produser` */

insert  into `produser`(`kd_produser`,`nm_produser`,`internasional`) values 
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PRAMOUNT PICTURE','YA');

/*Table structure for table `propinsi` */

DROP TABLE IF EXISTS `propinsi`;

CREATE TABLE `propinsi` (
  `id` int(10) DEFAULT NULL,
  `nama` char(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `propinsi` */

insert  into `propinsi`(`id`,`nama`) values 
(1,'DKI jakarta'),
(2,'Jawa Barat'),
(3,'papua barat'),
(4,'sulawesi selatan'),
(5,'jawa timu');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
